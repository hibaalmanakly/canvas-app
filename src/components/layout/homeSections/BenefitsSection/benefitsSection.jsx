// Dependencies
import React from "react";

// Shared
import "./benefitsSection.scss";

const BenefitsSection = () => (
  <div className="benefits-container container-fluid p-0">
    <div className="row">
      <div className="benefits-right col-md-6 p-0 order-md-2"></div>
      <div className="benefits-left col-md-6 order-md-1">
        <h2>Intrinsicly synergistic benefits.</h2>
        <p>
          Enthusiastically morph unique web-readiness via impactful platforms.
          Intrinsicly matrix premium expertise for diverse expertise.
          Intrinsicly drive collaborative bandwidth for accurate testing.
        </p>
        <h2>Appropriately target customers.</h2>
        <p>
          Energistically incentivize front-end web services via enabled
          collaboration and idea-sharing. Conveniently whiteboard effective
          niche markets before flexible ROI.
          <br />
          Phosfluorescently build turnkey convergence without goal-oriented
          systems.
        </p>
        <h2>Completely exploit focused.</h2>
        <p>
          Continually enable leveraged users after functional web-readiness.
          Interactively conceptualize accurate resources whereas distinctive
          e-markets.
        </p>
      </div>
    </div>
  </div>
);

export default BenefitsSection;
