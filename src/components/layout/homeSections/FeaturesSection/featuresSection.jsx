// Dependencies
import React, { Component } from "react";

// Components
import FeatureCard from "../../../../shared/components/FeatureCard/featureCard";

// Shared
import "./featuresSection.scss";

class FeaturesSection extends Component {
  state = {
    features: [
      {
        icon: "map-marker-alt",
        title: "Anywhere & Anytime",
        description:
          "Appropriately reconceptualize timely convergence through resource maximizing collaboration and idea-sharing. Globally coordinate cross-platform products via interoperable models.",
      },
      {
        icon: "credit-card",
        title: "International Transactions",
        description:
          "Rapidiously actualize wireless benefits before resource-leveling quality vectors. Dramatically innovate progressive convergence without tactical schemas. Competently unleash distributed users whereas.",
      },
      {
        icon: "shield-alt",
        title: "100% Secured & Trustable",
        description:
          "Appropriately redefine market positioning leadership skills whereas client-based  outside the box thinking. Monotonectally engage next-generation leadership skills without one-to-one testing procedures.",
      },
      {
        icon: "book",
        title: "Detailed Documentation",
        description:
          "Compellingly evisculate impactful e-services for extensible resources. Interactively grow timely e-commerce rather than optimal expertise. Uniquely optimize impactful experiences rather than cooperative.",
      },
    ],
  };

  render() {
    return (
      <div className="features-container container">
        <div className="features-heading row">
          <div className="col-12">
            <p className="features-p-1">Why You Choose Us?</p>
            <h2 className="features-h2">
              Powerful & Best Features for Canvas App
            </h2>
            <p className="features-p-2">
              Dynamically underwhelm end-to-end experiences for premier
              methodologies. Objectively benchmark front-end bandwidth vis-a-vis
              flexible e-services. Globally drive business convergence without
              backward-compatible markets.
            </p>
          </div>
        </div>
        <div className="features-content row justify-content-around">
          {this.state.features.map((content, index) => (
            <div key={index} className="col-md-5">
              <FeatureCard content={content} />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default FeaturesSection;
