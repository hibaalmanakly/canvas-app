import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileAlt } from "@fortawesome/free-regular-svg-icons";

// Components
import Button from "../../../../shared/components/Button/button";

// Shared
import versionImg from "../../../../shared/images/versionImg.jpg";
import "./versionDetailsSection.scss";

class VersionDetailsSection extends Component {
  state = {
    buttons: [
      {
        icon: {
          package: "fa",
          name: "arrow-right",
        },
        text: "learn more",
        btnClass: "btn btn-version-more",
      },
    ],
  };
  render() {
    return (
      <div className="version-container container ">
        <div className="row d-flex align-items-center">
          <div className="version-left col-lg-5 col-md-6 mb-4 mb-md-0">
            <img src={versionImg} alt="canvas version details" />
          </div>
          <div className="version-right col-md-6 offset-lg-1">
            <div className="icon">
              <FontAwesomeIcon icon={faFileAlt} />
            </div>
            <h2>Version 6 is now 2x Faster.</h2>
            <p>
              Enthusiastically morph unique web-readiness via impactful
              platforms. Intrinsicly matrix premium expertise for diverse
              expertise. Intrinsicly drive collaborative bandwidth for accurate
              testing.
            </p>
            <div>
              <div className="version">Version 6.0</div>
              <div className="skills">
                <div className="progress skills-animated">
                  <div className="progress-percent">
                    <div className="counter counter-inherit counter-instant">
                      <span
                        data-from="0"
                        data-to="60"
                        data-refresh-interval="30"
                        data-speed="1500"
                      >
                        60
                      </span>
                      % Optimized
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {this.state.buttons.map((content, index) => (
              <Button key={index} content={content} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
export default VersionDetailsSection;
