// Dependencies
import React from "react";

// Shared
import iconAmazon from "../../../../shared/images/icon-amazon.svg";
import iconNetflix from "../../../../shared/images/icon-netflix.svg";
import iconGoogle from "../../../../shared/images/icon-google.svg";
import iconLinkedIn from "../../../../shared/images/icon-linkedin.svg";
import "./clientsSection.scss";

const ClientsSection = () => (
  <div className="clients-container container">
    <div className="clients-row row">
      <div className="col-lg-8  ">
        <div className="client-icon">
          <img className="icon" src={iconAmazon} alt="amazon" />
        </div>
        <div className="client-icon">
          <img className="icon" src={iconNetflix} alt="netflix" />
        </div>
        <div className="client-icon">
          <img className="icon" src={iconGoogle} alt="google" />
        </div>
        <div className="client-icon">
          <img className="icon" src={iconLinkedIn} alt="linkedin" />
        </div>
      </div>
    </div>
  </div>
);

export default ClientsSection;
