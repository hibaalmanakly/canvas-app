// Dependencies
import React, { Component } from "react";

// Components
import Button from "../../../../shared/components/Button/button";

// Shared
import mainImg1 from "../../../../shared/images/mainImg1.png";
import mainImg2 from "../../../../shared/images/mainImg2.png";
import "./mainSection.scss";

class MainSection extends Component {
  state = {
    buttons: [
      {
        icon: {
          package: "fab",
          name: "apple",
        },
        text: "Get it on the App Store",
        btnClass: "btn btn-main-section",
      },
      {
        icon: {
          package: "fab",
          name: "google-play",
        },
        text: "Get it on Play Store",
        btnClass: "btn btn-main-section",
      },
    ],
  };
  render() {
    return (
      <div className="main-container container">
        <div className="row">
          <div className="col-md-6">
            <h2 className="main-h2">
              Our Design
              <br />
              Your Business.
            </h2>
            <p className="p-main">
              Best Wallet App for your upcoming Projects.
            </p>
            <div className="d-flex">
              {this.state.buttons.map((content, index) => (
                <Button key={index} content={content} />
              ))}
            </div>
            <p className="p-main-sec">
              <small>Sign up &amp; Get 30 Days Free trail</small>
            </p>
          </div>
          <div className="col-md-6 main-section-right">
            <img className="main-img-1" src={mainImg1} alt="canvas visa" />
            <img className="main-img-2" src={mainImg2} alt="canvas mobile" />
          </div>
        </div>
      </div>
    );
  }
}

export default MainSection;
