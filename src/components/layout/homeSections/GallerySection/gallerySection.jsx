// Dependencies
import React, { Component } from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";

// Shared
import galleryImg1 from "../../../../shared/images/gallery/1.png";
import galleryImg2 from "../../../../shared/images/gallery/2.png";
import galleryImg3 from "../../../../shared/images/gallery/3.png";
import galleryImg4 from "../../../../shared/images/gallery/4.png";
import galleryImg5 from "../../../../shared/images/gallery/5.png";
import galleryImg6 from "../../../../shared/images/gallery/6.png";
import galleryImg7 from "../../../../shared/images/gallery/7.png";
import deviceImg from "../../../../shared/images/gallery/device.png";
import "./gallerySection.scss";

class GallerySection extends Component {
  // TODO : create loop for slider images

  /** Items per device width */
  responsive = {
    0: {
      items: 1,
    },
    760: {
      items: 3,
    },
  };

  render() {
    return (
      <div className="gallery-container container-fluid">
        <div className="gallery-heading row">
          <div className="col-12">
            <p className="gallery-p-1">GALLERY</p>
            <h2 className="gallery-h2">App Screenshots</h2>
            <p className="gallery-p-2">
              Recusandae ducimus, sequi, sapiente possimus optio impedit
              efficient services. Completely promote.
            </p>
          </div>
        </div>
        <div className="gallery-content row justify-content-around">
          <img src={deviceImg} className="device-img" alt="" />
          <AliceCarousel
            autoPlayInterval={2000}
            infinite={true}
            autoPlay={true}
            buttonsDisabled={true}
            responsive={this.responsive}
          >
            <img src={galleryImg1} className="slider-img" alt="gallery-image" />
            <img src={galleryImg2} className="slider-img" alt="gallery-image" />
            <img src={galleryImg3} className="slider-img" alt="gallery-image" />
            <img src={galleryImg4} className="slider-img" alt="gallery-image" />
            <img src={galleryImg5} className="slider-img" alt="gallery-image" />
            <img src={galleryImg6} className="slider-img" alt="gallery-image" />
            <img src={galleryImg7} className="slider-img" alt="gallery-image" />
          </AliceCarousel>
        </div>
      </div>
    );
  }
}

export default GallerySection;
