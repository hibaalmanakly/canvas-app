// Dependencies
import React from "react";

// Components
import NavBar from "../../../shared/components/NavBar";

// Shared
import logo from "../../../shared/images/logo.png";
import "./header.scss";

const Header = () => (
  <header className="header">
    <div className="container">
      <div className="row d-flex align-items-center">
        <div className="logo col-6 p-0">
          <img className="" src={logo} alt="canvas logo" />
        </div>
        <div className="main-menu col-lg-6">
          <NavBar />
        </div>
      </div>
    </div>
  </header>
);

export default Header;
