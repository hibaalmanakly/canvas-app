// Dependencies
import React, { Component } from "react";

// Components
import Button from "../../../shared/components/Button/button";

// Shared
import "./footer.scss";

class Footer extends Component {
  state = {
    buttons: [
      {
        icon: {
          package: "fab",
          name: "apple",
        },
        text: "App Store",
        btnClass: "btn btn-footer-section",
      },
      {
        icon: {
          package: "fab",
          name: "google-play",
        },
        text: "Play Store",
        btnClass: "btn btn-footer-section",
      },
    ],
  };
  render() {
    return (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              Copyrights © 2021 All Rights Reserved by Canvas Inc.
              <br />
              <div className="copyright-links">
                <a href="https://www.google.com/">Terms of Use</a>
                <span>/</span>
                <a href="https://www.google.com/">Privacy Policy</a>
              </div>
            </div>
            <div className="col-md-6 text-right">
              {this.state.buttons.map((content, index) => (
                <Button key={index} content={content} />
              ))}
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
