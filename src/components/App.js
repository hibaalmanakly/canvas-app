// Dependencies
import React from "react";

// Components
import Header from "./layout/Header";
import Footer from "./layout/Footer";
import MainSection from "./layout/homeSections/MainSection";
import ClientsSection from "./layout/homeSections/ClientsSection";
import FeaturesSection from "./layout/homeSections/FeaturesSection";
import BenefitsSection from "./layout/homeSections/BenefitsSection";
import VersionDetailsSection from "./layout/homeSections/VersionDetailsSection";
import GallerySection from "./layout/homeSections/GallerySection";

// Shared
import "./App.scss";

function App() {
  return (
    <div className="container-fluid p-0">
      <section className="section-main">
        <Header />
        <MainSection />
      </section>
      <section className="section-clients">
        <ClientsSection />
      </section>
      <section className="section-features">
        <FeaturesSection />
      </section>
      <section className="section-benefits">
        <BenefitsSection />
      </section>
      <section className="section-version">
        <VersionDetailsSection />
      </section>
      <section className="section-gallery">
        <GallerySection />
      </section>
      <Footer />
    </div>
  );
}

export default App;
