// Dependencies
import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileAlt } from "@fortawesome/free-regular-svg-icons";
import { faChevronDown, faBars } from "@fortawesome/free-solid-svg-icons";

// Shared
import "./navBar.scss";

class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon">
            <FontAwesomeIcon icon={faBars} />
          </span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <a className="nav-link" href="https://www.google.com/">
                Home <span className="sr-only">(current)</span>
              </a>
            </li>
            <li className="nav-item dropdown">
              <div
                className="nav-link dropdown-toggle"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Features
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <a className="dropdown-item" href="https://www.google.com/">
                  <FontAwesomeIcon icon={faFileAlt} />
                  Products
                </a>
                <a className="dropdown-item" href="https://www.google.com/">
                  <FontAwesomeIcon icon={faFileAlt} />
                  Downloads
                </a>
                <a className="dropdown-item" href="https://www.google.com/">
                  <FontAwesomeIcon icon={faFileAlt} />
                  Support
                </a>
              </div>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://www.google.com/">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://www.google.com/">
                Pricing
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://www.google.com/">
                Contact
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
