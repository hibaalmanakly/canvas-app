// Dependencies
import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMapMarkerAlt,
  faCreditCard,
  faShieldAlt,
  faBook,
} from "@fortawesome/free-solid-svg-icons";

// Shared
import "./featureCard.scss";

library.add(faMapMarkerAlt, faCreditCard, faShieldAlt, faBook);

class FeatureCard extends Component {
  render() {
    const icon = this.props.content.icon;
    const title = this.props.content.title;
    const description = this.props.content.description;

    return (
      <React.Fragment>
        <div className="feature-icon">
          <FontAwesomeIcon icon={["fa", icon]} />
        </div>
        <h2 className="feature-title">{title}</h2>
        <p className="feature-desc">{description}</p>
      </React.Fragment>
    );
  }
}

export default FeatureCard;
