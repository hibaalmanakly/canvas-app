// Dependencies
import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faApple, faGooglePlay } from "@fortawesome/free-brands-svg-icons";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

// Shared
import "./button.scss";

library.add(faApple, faGooglePlay, faArrowRight);

class Button extends Component {
  render() {
    const iconPackage = this.props.content.icon.package;
    const iconName = this.props.content.icon.name;
    const btnClass = this.props.content.btnClass;
    const btnText = this.props.content.text;

    return (
      <a className={btnClass} href="https://www.google.com">
        <FontAwesomeIcon icon={[iconPackage, iconName]} />
        {btnText}
      </a>
    );
  }
}
export default Button;
